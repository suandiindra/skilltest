const express = require('express')
const mKategori = require('../models/mKategori')

const findKategori = async (req, res) =>{
    try {
        let response;
        response = await mKategori.findAll();
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({msg: error.message});
    }
}
const createKategori = async (req, res) => {
    try {
        const { nama_kategori, isactive } = req.body;
        const newCategory = await mKategori.create(req.body);
        if(newCategory){
            res.status(200).json({msg:`Berhasil`});
        }
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};

module.exports = {findKategori,createKategori}