const express = require('express')
const mBuku = require('../models/mBuku.js')
const mysql = require("mysql2/promise")
const {db,dbConfig} = require('../config/Databese.js');

const find = async (req, res) =>{
    try {
        let response;
        response = await mBuku.findAll();
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({msg: error.message});
    }
}
const createBuku = async (req, res) => {
    try {
        const { title, year_publish,author,m_kategori_id,isactive } = req.body;
        const hasil = await mBuku.create(req.body);
        if(hasil){
            res.status(200).json({msg:`Berhasil`});
        }
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};
const updateBuku = async (req, res) => {
    try {
        const { title, year_publish,author,m_kategori_id,isactive,m_buku_id } = req.body;
        // const hasil = await mBuku.create(req.body);
        let hasil = await mBuku.update({
            title, year_publish,author,m_kategori_id,isactive
        },{
            where:{
                m_buku_id: m_buku_id
            }
        });
        if(hasil){
            res.status(200).json({msg:`Berhasil`});
        }
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};
const searching = async (req, res) => {
    try {
        const {key} = req.query;
        const request = await mysql.createConnection(dbConfig);
        let qr = `select author,title,year_publish,b.nama_kategori from m_buku a 
        inner join m_kategori b on b.m_kategori_id = a.m_kategori_id
        where a.isactive = 'Y'
        and (title like '%${key}%' or year_publish like '%${key}%' or author like '%${key}%')`

        console.log(qr);
        let [sel] =  await request.execute(qr)

        res.status(200).json(sel);
        
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};
const groupByYear = async (req, res) => {
    try {
        const {group} = req.query;
        const request = await mysql.createConnection(dbConfig);
        let qr = ``
        console.log(group);
        if(group == 'Year'){
            qr = `select year_publish from m_buku where isactive = 'Y' group by year_publish`
        }else if(group == 'Author'){
            qr = `select author from m_buku where isactive = 'Y' group by author`
        }
        
        let [data] =  await request.execute(qr)
        for(let i = 0; i<data.length ; i ++){
            let filter = ``
            let dataFilter = ``
            if(group == 'Year'){
                dataFilter = data[i].year_publish
                filter = `and year_publish = '${dataFilter}'`
            }else if(group == 'Author'){
                dataFilter = data[i].author
                filter = `and author = '${dataFilter}'`
            }
            let dtx = `select * from m_buku where isactive = 'Y' ${filter}`
            console.log(dtx);
            let [objData] =  await request.execute(dtx)
            data[i] = {... data[i],data : objData}
        }

        res.status(200).json(data);
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: error.message });
    }
};
const deleteBuku = async (req, res) => {
    try {
        const {m_buku_id} = req.body;
        const hasil = await mBuku.destroy({
            where: {
              m_buku_id: m_buku_id,
            },
        });
        if(hasil){
            res.status(200).json({msg:`Berhasil`});
        }
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
};
module.exports = {createBuku,find,searching,updateBuku,groupByYear,deleteBuku}
