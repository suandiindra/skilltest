const Sequelize = require('sequelize')
const {db} = require('../config/Databese')
const mKategori = require('./mKategori')
const {DataTypes} = Sequelize;
const mBuku = db.define('m_buku',{
    m_buku_id:{
        type: DataTypes.STRING,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true, 
        validate:{
            notEmpty: true
        }
    },
    title:{
        type: DataTypes.STRING,
        allowNull: false,
        length : [100],
        validate:{
            len: [1, 200]
        }
    },
    year_publish:{
        type: DataTypes.STRING,
        allowNull: false,
        length : [10],
        validate:{
            len: [1, 200]
        }
    },
    author:{
        type: DataTypes.STRING,
        allowNull: false,
        length : [40],
        validate:{
            len: [1, 200]
        }
    },
    m_kategori_id:{
        type: DataTypes.STRING,
        allowNull: false,
        length : [40],
        validate:{
            len: [1, 200]
        },
        references: {
            model: mKategori,
            key: 'm_kategori_id', 
        },
    },
    isactive:{
        type: DataTypes.STRING,
        allowNull: true,
        validate:{
            len: [1]
        }
    },
},{
    freezeTableName: true
});

module.exports = mBuku