const Sequelize = require('sequelize')
const {db} = require('../config/Databese')

const {DataTypes} = Sequelize;
const mKategori = db.define('m_kategori',{
    m_kategori_id:{
        type: DataTypes.STRING,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true, 
        validate:{
            notEmpty: true
        }
    },
    nama_kategori:{
        type: DataTypes.STRING,
        allowNull: false,
        validate:{
            len: [1, 200]
        }
    },
    isactive:{
        type: DataTypes.STRING,
        allowNull: true,
        validate:{
            len: [1]
        }
    },
},{
    freezeTableName: true
});

module.exports = mKategori