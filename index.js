const express = require("express");
const cors = require("cors");
const http = require("http");
const app = express();
const server = http.createServer(app);
const bodyParser = require("body-parser");
const {db,dbConfig} = require('./config/Databese');
const mKategori = require("./models/mKategori")
const mBuku = require("./models/mBuku")
const entriRoute = require("./routes/entry")

const PORT = process.env.PORT || 7000;
app.use(express.json());

app.use(
    bodyParser.urlencoded({
        extended: false,
    })
);

db.sync().then(() => {
    console.log('Database synchronized');
}).catch(error => {
    console.error('Error syncing database:', error);
});
app.use("/",entriRoute);

server.listen(PORT, () => {
    console.log(`The Service running well...in http://localhost:${PORT}`);
});
