const Sequelize = require('sequelize')

const db = new Sequelize('skilltest_db', 'root', '', {
    host: "localhost",
    dialect: "mysql"
});

const dbConfig = {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'skilltest_db'
  };

module.exports = {db,dbConfig}