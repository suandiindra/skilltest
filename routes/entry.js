const express = require("express");
const router = express.Router();
const KategoriController = require('../controllers/kategoriController')
const bukuController = require('../controllers/bukuController')

router.get('/kategori/find',KategoriController.findKategori);
router.post('/kategori/create',KategoriController.createKategori);
router.post('/buku/create',bukuController.createBuku);
router.post('/buku/update',bukuController.updateBuku);
router.post('/buku/deleteBuku',bukuController.deleteBuku);
router.get('/buku/find',bukuController.find);
router.get('/buku/searching',bukuController.searching);
router.get('/buku/groupByYear',bukuController.groupByYear);

module.exports = router
